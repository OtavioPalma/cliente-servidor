// MessageListener is an interface for classes that wish to
// receive new chat messages.
package com.deitel.messenger;

public interface MessageListener 
{
   // receive new chat message
   public void messageReceived(String from, String message);
}  // end interface MessageListener