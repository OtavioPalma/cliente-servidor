// DeitelMessenger is a chat application that uses a ClientGUI
// and SocketMessageManager to communicate with DeitelMessengerServer.
package com.deitel.messenger.sockets.client;

import com.deitel.messenger.MessageManager;
import com.deitel.messenger.ClientGUI;

public class DeitelMessenger
{   
   public static void main(String args[]) 
   {
      MessageManager messageManager; // declare MessageManager
      
      if (args.length == 0)
         // connect to localhost
         messageManager = new SocketMessageManager("172.16.104.210");
      else
         // connect using command-line arg
         messageManager = new SocketMessageManager(args[ 0 ]);  
      
      // create GUI for SocketMessageManager
      ClientGUI clientGUI = new ClientGUI(messageManager);
      clientGUI.setSize(300, 400); // set window size
      clientGUI.setResizable(false); // disable resizing
      clientGUI.setVisible(true); // show window
   }
}