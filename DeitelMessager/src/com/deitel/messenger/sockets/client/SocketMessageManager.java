// SocketMessageManager communicates with a DeitelMessengerServer using 
// Sockets and MulticastSockets.
package com.deitel.messenger.sockets.client;

import java.net.InetAddress;
import java.net.Socket;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.deitel.messenger.MessageListener;
import com.deitel.messenger.MessageManager;
import static com.deitel.messenger.sockets.SocketMessengerConstants.*;

public class SocketMessageManager implements MessageManager
{
   private Socket clientSocket; // Socket for outgoing messages
   private String serverAddress; // DeitelMessengerServer address
   private PacketReceiver receiver; // receives multicast messages
   private boolean connected = false; // connection status
   private ExecutorService serverExecutor; // executor for server
   
   public SocketMessageManager(String address)
   {
      serverAddress = address; // store server address
      serverExecutor = Executors.newCachedThreadPool();
   }
   
   // connect to server and send messages to given MessageListener
   public void connect(MessageListener listener) 
   {
      if (connected)
         return; // if already connected, return immediately

      try // open Socket connection to DeitelMessengerServer
      {
         clientSocket = new Socket(
            InetAddress.getByName(serverAddress), SERVER_PORT);

         // create runnable for receiving incoming messages
         receiver = new PacketReceiver(listener);
         serverExecutor.execute(receiver); // execute runnable
         connected = true; // update connected flag
      }
      catch (IOException ioException) 
      {
         ioException.printStackTrace();
      }
   }
   
   // disconnect from server and unregister given MessageListener
   public void disconnect(MessageListener listener) 
   {
      if (!connected)
         return; // if not connected, return immediately
      
      try // stop listener and disconnect from server
      {     
         // notify server that client is disconnecting
         Runnable disconnecter = new MessageSender(clientSocket, "", 
            DISCONNECT_STRING);         
         Future disconnecting = serverExecutor.submit(disconnecter);         
         disconnecting.get(); // wait for disconnect message to be sent
         receiver.stopListening(); // stop receiver
         clientSocket.close(); // close outgoing Socket
      }
      catch (ExecutionException exception) 
      {
         exception.printStackTrace();
      }
      catch (InterruptedException exception) 
      {
         exception.printStackTrace();
      }
      catch (IOException ioException) 
      {
         ioException.printStackTrace();
      }
     
      connected = false; // update connected flag
   }
   
   // send message to server
   public void sendMessage(String from, String message) 
   {
      if (!connected)
         return; // if not connected, return immediately
      
      // create and start new MessageSender to deliver message
      serverExecutor.execute(
         new MessageSender(clientSocket, from, message));
   }
}