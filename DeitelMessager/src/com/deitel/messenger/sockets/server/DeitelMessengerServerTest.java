// Test the DeitelMessengerServer class.
package com.deitel.messenger.sockets.server;

public class DeitelMessengerServerTest 
{   
   public static void main (String args[]) 
   {
      DeitelMessengerServer application = new DeitelMessengerServer();
      application.startServer(); // start server
   }
}