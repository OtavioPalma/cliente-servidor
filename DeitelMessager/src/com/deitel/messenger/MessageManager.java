// MessageManger is an interface for objects capable of managing
// communications with a message server.
package com.deitel.messenger;

public interface MessageManager 
{      
   // conecta ao servidor de mensagens e roteia
   // mensagens recebidas para o MessageListener
   public void connect(MessageListener listener);
   
   // desconecta ao servidor de mensagens e para de
   // rotear mensagens recebidas para o MessageListener
   public void disconnect(MessageListener listener);
   
   // envia mensagens para o servidor de mensagens
   public void sendMessage(String from, String message);  
}